const express = require('express');
const router = express.Router();

const db = require('../../dbmanager');
const bcrypt = require('bcrypt-nodejs');

const jwt = require('jsonwebtoken');



router.post('/auth/check', (req, res) => {
    const token = req.body.token;
    let jwt_secret = "OmyGirl";



    if(!token) {
        res.json({
            result: "fail",
            msg: "No Token"
        });
    }

    const checkToken = new Promise((resolve, reject) => {
        jwt.verify(token, jwt_secret, (err, decoded) => {
            if(err) {
                console.log("jwt verify err!");
                reject(err);
            }
            resolve(decoded);
        });
    });


    checkToken.then((data) => {
        res.json({
            result: "success",
            token: data
        });
    },
    (data) => {
        res.json({
            result: "fail",
            msg: data.message
        });
    });
    
});



router.post('/login', (req, res) => {
    const user_id = req.body.user_email;
    const user_pw = req.body.password;
  
    console.log('Login request!');
    db.getConnection((err, connection) => { 
        if ( err ) {
            console.log("db Err1!");
            throw err;
        }
            
        else {
            console.log("User login db Request!");
            const query = `SELECT * FROM USERS WHERE email = '${user_id}'`;
            // 커넥션 사용
            connection.query(query, (err, results) => {

                let jwt_secret = "OmyGirl";
                
                if (err) {
                    console.log("db Err2!");
                    const redirectURL = 'http://localhost:3000/login';
                    res.redirect(redirectURL);
                    throw err; 
                } else {
                    // connect!
                    if(results.length === 0) {
                        // 존재하지 않는 경우
                        res.json({result: "fail", msg: "존재하지 않는 유저입니다."});
                    } else {
                        const payload = {
                            id: results[0].id
                        };
                        // 존재하는 경우!
                        if(!bcrypt.compareSync(user_pw, results[0].password)) {
                            res.json({result: "fail", msg: "비밀번호가 일치하지 않습니다."});
                        } else {


                            console.log("token ---");

                            const getToken = new Promise((resolve, reject) => {
                                jwt.sign({
                                    id: user_id,
                                    role: "user"
                                },
                                jwt_secret, {
                                    expiresIn: '1h', //유효 시간
                                    issuer: 'Mimi', // 토큰 발급자
                                    subject: 'arin' // 토큰 발급 대상
                                }, (err, token) => {
                                    if(err) 
                                        reject(err);
                                    resolve(token);
                                })
                            });
                            
                            getToken.then(
                                token => {
                                    res.status(200).json({
                                        result: "success",
                                        token: token,
                                        id: results[0].name    
                                    });

                                }
                            )
                            //--------------------------------------------------------------------    
                        }
                    }
                }
            });
            // 커넥션 반환 ( 커넥션 종료 메소드가 커넥션과 다르다 )
            connection.release() 
        }
    });


    
  });
 










// 회원가입 기능
router.post('/register', (req, res) => {
    
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
    res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization, Content-Length, X-Requested-With');


    db.getConnection( function( err, connection ) {  



        if (err) {
            throw err;
            console.log("db Err1!");
        }
            
        else {
            const loc = 'http://localhost:3000/login';
            const password = req.body.password;
            bcrypt.hash(password , null, null,(err, hash) => {
                const query = `INSERT INTO USERS(email, password, name, phone_number, birthday, register_time) values ('${req.body.email}', '${hash}', '${req.body.realname}', '${req.body.tel_number}', '${req.body.birthday}', now())`;
            // 커넥션 사용
            connection.query(query, function( err, results ) 
            {
                if (err) {
                    throw err;
                    console.log("db Err2!");
                } 
                    
                else {
                    console.log("insert db Success");
                    res.redirect(loc);
                }
                   
            });
            });
            
            // 커넥션 반환 ( 커넥션 종료 메소드가 커넥션과 다르다 )
            connection.release() 
        }
    });

});

// 유저 데이터 불러오기
router.post('/list', (req, res, next) => {



    const token = req.body.token;
    let jwt_secret = "OmyGirl";



    if(!token) {
        res.json({
            result: "fail",
            msg: "No Token"
        });
    }

    const checkToken = new Promise((resolve, reject) => {
        jwt.verify(token, jwt_secret, (err, decoded) => {
            if(err) {
                console.log("jwt verify err!");
                reject(err);
            }
            resolve(decoded);
        });
    });


    checkToken.then((data) => {
        
        db.getConnection((err, connection) => {  
            if ( err ) {
                console.log("db Err1!");
                throw err;
            }
                
            else 
            {
                // 커넥션 사용
                connection.query("SELECT * FROM users", function( err, results ) 
                {
                    if (err) {
                        throw err;
                        console.log("db Err2!");
                    } 
                        
                    else {
                        console.log("select db Success");
                        res.json({
                            result: "success",
                            data: results
                        });
                    }
                       
                });
                // 커넥션 반환 ( 커넥션 종료 메소드가 커넥션과 다르다 )
                connection.release() 
            }
        });
    },
    (data) => {
        res.json({
            result: "fail",
            msg: data.message
        });
    });



   
});

// 모듈로 만들어준다. app.js에서 use로 api/user 경로는 모두 여기서 처리하게끔 했기 때문에 모듈로 만들어 주어야한다.
module.exports = router;