const express = require('express');
const router = express.Router();

const db = require('../../dbmanager');
const bcrypt = require('bcrypt-nodejs');

const jwt = require('jsonwebtoken');


// 토큰 유효성 검사!
router.get('/auth/check', (req, res) => {
  
    const token = req.headers['token'] || req.query.token;
    let jwt_secret = "OmyGirl";
    console.log("-----------req token---------------");
    console.log(token);

    if(!token) {
        res.json({
            result: "fail",
            msg: "No Token"
        });
    }

    const checkToken = new Promise((resolve, reject) => {
        jwt.verify(token, jwt_secret, (err, decoded) => {
            if(err) {
                console.log("jwt verify err!");
                reject(err);
            }
            resolve(decoded);
        });
    });


    checkToken.then(
        token => {
            console.log(token);
            res.json({
                result: "success",
                token: token
            });
        }
    )


});


module.exports = router;